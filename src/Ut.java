public class Ut {
	
    static String hexStr(int num, int len) {
        String hexNum = Integer.toHexString(num);
		if( num < 0 ) hexNum = hexNum.substring(4,8);
        while(hexNum.length() < len) {
            hexNum = "0" + hexNum;
        }
        return "0x" + hexNum.toUpperCase();
    }

    static String binStr(int num, int len) {
        String binNum = Integer.toBinaryString(num), rt = "";
		if( num < 0 ) binNum = binNum.substring(16,32);
        while(binNum.length() < len) {
            binNum = "0" + binNum;
        }
        /*for(int i=0; i<binNum.length(); i+=8) {
            if(i > 0) rt = rt + " ";
            rt = rt + binNum.substring(i,i+8);
        }*/
        return binNum;
    }

}

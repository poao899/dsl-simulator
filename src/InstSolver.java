import java.util.*;

public class InstSolver {

	static String MR = "Memory Read";
	static String MDM = "Memory Data in MDR";
	static String MW = "Memory Write";
	static int rx, ry;

	static boolean isLoadRximm;
	static boolean isLoadRxmem;
	static boolean isStoremem;
	static boolean isJump, isJumpC, isJumpNC;

	static void solve(int inst, LinkedList<String> que) {

		String si = Ut.binStr(inst, 16);	

		// check the flag

		if( isLoadRximm ) {
			// Load rx, #imm
			que.add("= Load R" + rx + ", #" + inst);
			que.add("PC -> MAR");
			que.add(MR);
			que.add(MDM);
			que.add("PC_INC");
			que.add("MDR -> R" + rx);
			isLoadRximm = false;
		}

		else if( isLoadRxmem ) {
			// Load rx, [mem]
			que.add("= Load R" + rx + ", [" + inst + "]");
			que.add("PC -> MAR");
			que.add(MR);
			que.add(MDM);
			que.add("PC_INC");
			que.add("MDR -> MAR");
			que.add(MR);
			que.add(MDM);
			que.add("MDR -> R" + rx);
			isLoadRxmem = false;
		}

		else if( isStoremem ) {
			// Store [mem], Rx
			que.add("= Store [" + inst + "], R" + rx);
			que.add("PC -> MAR");
			que.add(MR);
			que.add(MDM);
			que.add("PC_INC");
			que.add("MDR -> MAR");
			que.add("R" + rx + " -> MDR");
			que.add(MW);
			isStoremem = false;
		}

		else if( isJump ) {
			// Jump
			que.add("= Jump [" + inst + "]");
			que.add("PC -> MAR");
			que.add(MR);
			que.add(MDM);
			que.add("MDR -> PC");
			isJump = false;
		}

		else if( isJumpC ) {
			// Jump C
			que.add("= JumpC [" + inst + "]");
			que.add("PC -> MAR");
			que.add(MR);
			que.add(MDM);
			que.add("JumpC");
			isJumpC = false;
		}

		else if( isJumpNC ) {
			// Jump NC
			que.add("= JumpNC [" + inst + "]");
			que.add("PC -> MAR");
			que.add(MR);
			que.add(MDM);
			que.add("JumpNC");
			isJumpNC = false;
		}

		else {

			// instruction fetch
			que.add("> Instruction Fetch");
			que.add("PC -> MAR");
			que.add(MR);
			que.add(MDM);
			que.add("MDR -> IR");
			que.add("PC_INC");

			// decode
			que.add("> Decode");
			que.add("IR -> Instruction Decoder");

			// no flag

			if(si.substring(0,7).equals("0001000")) {
				// 0001_000x_xxyy_y000
				rx = Integer.parseInt( si.substring(7 ,10), 2 )+1;
				ry = Integer.parseInt( si.substring(10,13), 2 )+1;
				que.add("= Add R" + rx + ", R" + ry);
				que.add("R" + rx + " -> X");
				que.add("R" + ry + " -> Y");
				que.add("ALU do W = X + Y");
				que.add("W -> R" + rx);
			}

			else if(si.substring(0,7).equals("0001001")) {
				// 0001_001x_xxyy_y000
				rx = Integer.parseInt( si.substring(7 ,10), 2 )+1;
				ry = Integer.parseInt( si.substring(10,13), 2 )+1;
				que.add("= Sub R" + rx + ", R" + ry);
				que.add("R" + rx + " -> X");
				que.add("R" + ry + " -> Y");
				que.add("ALU do W = X - Y");
				que.add("W -> R" + rx);
			}

			else if(si.substring(0,7).equals("0001010")) {
				// 0001_010x_xxyy_y000
				rx = Integer.parseInt( si.substring(7 ,10), 2 )+1;
				ry = Integer.parseInt( si.substring(10,13), 2 )+1;
				que.add("= Mul R" + rx + ", R" + ry);
				que.add("R" + rx + " -> X");
				que.add("R" + ry + " -> Y");
				que.add("ALU do W = X * Y");
				que.add("W -> R" + rx);
			}

			else if(si.substring(0,7).equals("0001011")) {
				// 0001_011x_xxyy_y000
				rx = Integer.parseInt( si.substring(7 ,10), 2 )+1;
				ry = Integer.parseInt( si.substring(10,13), 2 )+1;
				que.add("= Div R" + rx + ", R" + ry);
				que.add("R" + rx + " -> X");
				que.add("R" + ry + " -> Y");
				que.add("ALU do W = X / Y");
				que.add("W -> R" + rx);
			}


			else if(si.substring(0,7).equals("0001100")) {
				// 0001_100x_xxyy_y000
				rx = Integer.parseInt( si.substring(7 ,10), 2 )+1;
				ry = Integer.parseInt( si.substring(10,13), 2 )+1;
				que.add("= Move R" + rx + ", R" + ry);
				que.add("R" + ry + " -> R" + rx);
			}

			else if(si.substring(0,7).equals("0001110")) {
				// 0001_110x_xxyy_y000
				rx = Integer.parseInt( si.substring(7 ,10), 2 )+1;
				ry = Integer.parseInt( si.substring(10,13), 2 )+1;
				que.add("= Swap R" + rx + ", R" + ry);
				que.add("R" + rx + " -> Temp");
				que.add("R" + ry + " -> R" + rx);
				que.add("Temp" + " -> R" + ry);
			}	

			else if(si.substring(0,7).equals("0010001")) {
				// 0010_001x_xxyy_y000
				rx = Integer.parseInt( si.substring(7 ,10), 2 )+1;
				ry = Integer.parseInt( si.substring(10,13), 2 )+1;
				que.add("= And R" + rx + ", R" + ry);
				que.add("R" + rx + " & R" + ry + " -> R" + rx);
			}

			else if(si.substring(0,7).equals("0010010")) {
				// 0010_010x_xxyy_y000
				rx = Integer.parseInt( si.substring(7 ,10), 2 )+1;
				ry = Integer.parseInt( si.substring(10,13), 2 )+1;
				que.add("= Or R" + rx + ", R" + ry);
				que.add("R" + rx + " | R" + ry + " -> R" + rx);
			}

			else if(si.substring(0,7).equals("0010100")) {
				// 0010_100x_xxyy_y000
				rx = Integer.parseInt( si.substring(7 ,10), 2 )+1;
				ry = Integer.parseInt( si.substring(10,13), 2 )+1;
				que.add("= Xor R" + rx + ", R" + ry);
				que.add("R" + rx + " ^ R" + ry + " -> R" + rx);
			}

			else if(si.substring(0,7).equals("0100001")) {
				// 0100_001x_xx00_0000
				rx = Integer.parseInt( si.substring(7 ,10), 2 )+1;
				isLoadRximm = true;
			}

			else if(si.substring(0,7).equals("0100010")) {
				// 0100_010x_xx00_0000
				rx = Integer.parseInt( si.substring(7 ,10), 2 )+1;
				isLoadRxmem = true;
			}
			else if(si.substring(0,7).equals("0100011")) {
				// 0100_011x_xxyy_y000
				rx = Integer.parseInt( si.substring(7 ,10), 2 )+1;
				ry = Integer.parseInt( si.substring(10,13), 2 )+1;
				que.add("= Load R" + rx + ", [R" + ry + "]");
				que.add("R" + ry + " -> MAR");
				que.add(MR);
				que.add(MDM);
				que.add("MDR -> R" + rx);
			}
			else if(si.substring(0,7).equals("0100100")) {
				// 0100_100x_xx00_0000	
				rx = Integer.parseInt( si.substring(7 ,10), 2 )+1;
				isStoremem = true;
			}
			else if(si.equals("0011010000000000")) {
				// 0011_0100_0000_0000
				que.add("= Clear.C");
				que.add("0 -> Carry");
			}
			else if(si.equals("0011011000000000")) {
				// 0011_0110_0000_0000
				que.add("= Set.C");
				que.add("1 -> Carry");
			}
			else if(si.substring(0,7).equals("0011100")) {
				// 0011_100x_xx00_iiii
				rx = Integer.parseInt( si.substring(7,10), 2 )+1;
				int i = Integer.parseInt( si.substring(12,16), 2);
				que.add("= Test.biti R" + rx + ", with " + i);
				que.add("TestBit R" + rx + " " + i + " -> Carry");
			}
			else if(si.equals("1000001000000000")) {
				// Jump
				isJump = true;
			}
			else if(si.equals("1000011000000000")) {
				// Jump C
				isJumpC = true;
			}
			else if(si.equals("1000101000000000")) {
				// Jump NC
				isJumpNC = true;
			}
			else if(si.substring(0,7).equals("1100001")) {
				// Push Rx
				rx = Integer.parseInt( si.substring(7,10), 2 )+1;
				que.add("= Push R" + rx );
				que.add("SP -> MAR");
				que.add("R" + rx + " -> MDR");
				que.add(MW);
				que.add("SP_DEC");
			}
			else if(si.substring(0,7).equals("1100000")) {
				// Pop Rx
				rx = Integer.parseInt( si.substring(7,10), 2)+1;
				que.add("= Pop R" + rx );
				que.add("SP_INC");
				que.add("SP -> MAR");
				que.add(MR);
				que.add(MDM);
				que.add("MDR -> R" + rx);
			}
			else if(si.equals("1100010000000000")) {
				// Push PC
				que.add("= Push PC");
				que.add("SP -> MAR");
				que.add("PC_INC");
				que.add("PC_INC");
				que.add("PC -> MDR");
				que.add("PC_DEC");
				que.add("PC_DEC");
				que.add(MW);
				que.add("SP_DEC");
			}
			else if(si.substring(0,7).equals("1110000")) {
				// RSF
				rx = Integer.parseInt( si.substring(7 ,10), 2 )+1;
				ry = Integer.parseInt( si.substring(10,13), 2 )+1;
				que.add("= RSF R" + rx + ", R" + ry);
				que.add("RSF R" + rx + ", R" + ry);
			}
			else if(si.substring(0,7).equals("1110001")) {
				// LSF
				rx = Integer.parseInt( si.substring(7 ,10), 2 )+1;
				ry = Integer.parseInt( si.substring(10,13), 2 )+1;
				que.add("= LSF R" + rx + ", R" + ry);
				que.add("LSF R" + rx + ", R" + ry);
			}
			else if(si.substring(0,7).equals("1110010")) {
				// RSF
				rx = Integer.parseInt( si.substring(7 ,10), 2 )+1;
				ry = Integer.parseInt( si.substring(10,13), 2 )+1;
				que.add("= RSF3 R" + rx + ", R" + ry);
				que.add("RSF3 R" + rx + ", R" + ry);
			}
			else if(si.substring(0,7).equals("1110011")) {
				// LSF
				rx = Integer.parseInt( si.substring(7 ,10), 2 )+1;
				ry = Integer.parseInt( si.substring(10,13), 2 )+1;
				que.add("= LSF3 R" + rx + ", R" + ry);
				que.add("LSF3 R" + rx + ", R" + ry);
			}
		} // else end
	}
}

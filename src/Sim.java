import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.io.*;

public class Sim extends JFrame {
    
    final int wSize = 900, hSize = 470;
    final int memSize = 65536;
    public static final Font fonts = new Font(Font.MONOSPACED, Font.BOLD, 16);

    JTextArea memTable, insTable;
    JButton btIns, btStp;
    JScrollPane insScroll;
    JPanel ctrPane;
    int memPtr;
    int[] memArray;
    LinkedList<String> insQueue; 

	Simp16 myCpu = new Simp16(this);

    void memPaint() {
        String str = "";
        for(int i=0; i<20; i++) {
            int nowMemPtr = memPtr + i;
            if(nowMemPtr >= memSize) break;
			String val = Ut.binStr(memArray[nowMemPtr],16);
            str = str + Ut.hexStr(nowMemPtr,4) + " : " + val.substring(0,8) + " " + val.substring(8,16) + "\n";
        }
        memTable.setText(str);
    }

    void memInit() {
        memTable = new JTextArea();
        memTable.setFont(fonts);
        memTable.setForeground(new Color(100,100,100));
        memTable.setSize(wSize/3, hSize);
        memTable.setLocation(0,0);
        memTable.setBackground(new Color(200,200,255));
        memArray = new int[memSize];
        add(memTable);
        memPaint();
    }

    void insInit() {

        insTable = new JTextArea();
        insTable.setFont(fonts);
        insTable.setForeground(new Color(100,100,100));
        insTable.setSize(wSize/3, hSize);
        insTable.setLocation(0, 0);
        insTable.setBackground(new Color(255,200,200));
        
        insQueue = new LinkedList<String>();
        
        insScroll = new JScrollPane(insTable);
        insScroll.setSize(wSize/3, hSize);
        insScroll.setLocation(wSize*2/3, 0);
        insScroll.setVerticalScrollBarPolicy( ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS );
        
        add(insScroll);
    }

    void nextInst() {
        try {
			memPtr = myCpu.PC;
            int inst = memArray[memPtr];
            
     		// solve instruction
            InstSolver.solve(inst, insQueue);
			
			memPaint();
        } catch (Exception ex) {}
	}

    int stepCnt = 0;
    void nextStep() {
        if(insQueue.size() == 0)
            nextInst();
        String arg = insQueue.remove();
        
		// Perform a instruction
		myCpu.solve(arg);
		
		++stepCnt;
        if(arg.charAt(0) == '=')
			insTable.setText( insTable.getText() + (myCpu.PC) + ": " + arg + "\n" );
        //insTable.setCaretPosition(0);
    }

    void butInit() {
        ctrPane = new JPanel();
        ctrPane.setSize(wSize/3, hSize);
        ctrPane.setLocation(wSize/3, 0);
        ctrPane.setBackground(new Color(200,255,200));
        add(ctrPane);
        btIns = new JButton("Inst by Inst");
        btStp = new JButton("Step by Step");
        ctrPane.add(btIns);
        ctrPane.add(btStp);

        btIns.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ev) {
                nextInst();
                while(insQueue.size() > 0) {
                    nextStep();
                }
            }
        });
        btStp.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ev) {
                nextStep();
            }
        });
        
        JButton bt = new JButton("?");
        bt.addActionListener(new ActionListener() {
            boolean flag;
            public void actionPerformed(ActionEvent ev) {
                if( flag == false ) {
                    flag = true;
                    new Thread() {
                        Random rnd = new Random();
                        public void run() {
                            while(flag) {
                                memTable.setBackground(new Color(rnd.nextInt(256),rnd.nextInt(256),rnd.nextInt(256)));
                                memTable.setForeground(new Color(rnd.nextInt(256),rnd.nextInt(256),rnd.nextInt(256)));
                                insTable.setBackground(new Color(rnd.nextInt(256),rnd.nextInt(256),rnd.nextInt(256)));
                                insTable.setForeground(new Color(rnd.nextInt(256),rnd.nextInt(256),rnd.nextInt(256)));
                                ctrPane.setBackground(new Color(rnd.nextInt(256),rnd.nextInt(256),rnd.nextInt(256)));
                                try{Thread.sleep((rnd.nextInt(7)+1)*100);} catch(Exception ex) {ex.printStackTrace();}
                            }
                        }
                    }.start();
                }
                else {
                    flag = false;
                    memTable.setBackground(new Color(255,200,200));
                    memTable.setForeground(new Color(100,100,100));
                    insTable.setBackground(new Color(200,200,255));
                    insTable.setForeground(new Color(100,100,100));
                    ctrPane.setBackground(new Color(200,255,200));
                }
            }
        });
        ctrPane.add(bt);

		JButton btAdd = new JButton("Add");
		btAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ev) {
				String in;
				in = JOptionPane.showInputDialog("Input Position of the Data:");
				int pos = Integer.parseInt(in);
				in = JOptionPane.showInputDialog("Input Value of the Data:");
				int val = Integer.parseInt(in, 2);
				if(pos >= memSize || pos < 0) return ;
				memArray[pos] = val;
				memPaint();
			}
		});
		ctrPane.add(btAdd);
		JButton btOp = new JButton("Open");
		btOp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ev) {
				try {
					JFileChooser fc = new JFileChooser();
					int rt = fc.showOpenDialog(null);
					if(rt == JFileChooser.APPROVE_OPTION) {
						File file = fc.getSelectedFile();
						BufferedReader bf = new BufferedReader(new FileReader(file));
						String in;
						int cnt = 0;
						while(true) {
							in = bf.readLine();
							if(in == null) break;
							memArray[cnt] = Integer.parseInt(in,2);
							cnt ++;	
						}
					}
					myCpu.init();
					insTable.setText("");
					memPtr = 0;
					memPaint();
				} catch(Exception ex) {
					JOptionPane.showMessageDialog(null, "Error");
				}
			}
		});
    	ctrPane.add(btOp);
    	ctrPane.add(new JButton("new"));
    	ctrPane.add(new JButton("year"));
		ctrPane.add(myCpu.jt);
	}

    public Sim() {
        setSize(wSize, hSize+50);
        setLayout(null);
        memInit();
        insInit();
        butInit();
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    public static void main(String []args) {
        new Sim();
    }
}

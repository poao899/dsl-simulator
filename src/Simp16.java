import javax.swing.*;

public class Simp16 {
	int[] R = new int[9];
	int X, Y, W;
	int IR;
	int PC;
	int MDR, MAR;
	int TEMP;
	int SP;
	int Carry;

	static final int INT_MAX =  65535;
	static final int INT_MIN =  0;

	private Sim sim;

	public JTextArea jt = new JTextArea("");

	public void init() {	
		X = 0;
		Y = 0;
		W = 0;
		IR = 0;
		PC = 0;
		MDR = 0;
		MAR = 0;
		TEMP = 0;
		Carry = 0;
		SP = 65535;
		jt.setFont(Sim.fonts);
		repaint();
	}

	public Simp16(Sim _s) {
		sim = _s;
		init();
	}
	
	public void repaint() {
		String str = "";
		str += "__X_:" + Ut.binStr(X, 16) + " (" + X + ")\n";
		str += "__Y_:" + Ut.binStr(Y, 16) + " (" + Y + ")\n";
		str += "__W_:" + Ut.binStr(W, 16) + " (" + W + ")\n";
		str += "_IR_:" + Ut.binStr(IR, 16) + " (" + Ut.hexStr(IR,4) + ")\n";
		str += "_PC_:" + Ut.binStr(PC, 16) + " (" + Ut.hexStr(PC,4) + ")\n";
		str += "_MDR:" + Ut.binStr(MDR, 16) + " (" + Ut.hexStr(MDR,4) + ")\n";
		str += "_MAR:" + Ut.binStr(MAR, 16) + " (" + Ut.hexStr(MAR,4) + ")\n";
		for(int i=1; i<=8; i++) {
			str += "_R" + i + "_:" + Ut.binStr(R[i], 16) + " (" + R[i] + ")\n";
		}
		str += "TEMP:" + Ut.binStr(TEMP, 16) + " (" + TEMP + ")\n";
		str += "Carry:" + Carry + "\n";
		jt.setText(str);
	}
	
	public void solve(String arg) {
		
		String[] args = arg.split(" ");

		if(arg.charAt(0) == '=') {
			// Label, should be ignored
			System.out.println(arg);
		}
		else if(arg.charAt(0) == '>') {
			// Label, do nothing
		}
		else if(arg.equals(InstSolver.MR)) {
			// Memory Read
		}
		else if(arg.equals(InstSolver.MDM)) {
			// Memory data in MDR
			MDR = sim.memArray[MAR];
		}
		else if(arg.equals(InstSolver.MW)) {
			// Memory Write
			sim.memArray[MAR] = MDR;
		}
		else if(args.length == 3 && args[1].equals("->")) {
			
			// Assign operation
			int tmp = 0;
			// From
			if(args[0].equals("Y")) tmp = Y;
			else if(args[0].equals("X")) tmp = X;
			else if(args[0].equals("W")) tmp = W;
			else if(args[0].equals("IR")) tmp = IR;
			else if(args[0].equals("PC")) tmp = PC;
			else if(args[0].equals("MDR")) tmp = MDR;
			else if(args[0].equals("MAR")) tmp = MAR;
			else if(args[0].equals("Carry")) tmp = Carry;
			else if(args[0].equals("TEMP")) tmp = TEMP;
			else if(args[0].equals("0")) tmp = 0;
			else if(args[0].equals("1")) tmp = 1;
			else {
				for(int i=1; i<=8; i++)
					if(args[0].equals("R" + i)) tmp = R[i];
			}
			// To
			if(args[2].equals("Y")) Y = tmp;
			else if(args[2].equals("X")) X = tmp;
			else if(args[2].equals("W")) W = tmp;
			else if(args[2].equals("IR")) IR = tmp;
			else if(args[2].equals("PC")) PC = tmp;
			else if(args[2].equals("MDR")) MDR = tmp;
			else if(args[2].equals("MAR")) MAR = tmp;
			else if(args[2].equals("Carry")) Carry = tmp;
			else if(args[2].equals("TEMP")) TEMP = tmp;
			else {
				for(int i=1; i<=8; i++)
					if(args[2].equals("R" + i)) R[i] = tmp;
			}
		}
		else if(arg.equals("PC_INC")) {
			PC ++;
		}
		else if(arg.equals("PC_DEC")) {
			PC --;
		}
		else if(args.length == 7 && args[0].equals("ALU")) {
			// ALU
			switch( args[5].charAt(0) ) {
				case '+':
					W = X + Y;
					if( W > INT_MAX ) {
						W -= (INT_MAX+1);
						Carry = 1;
					}
					else if( W < INT_MIN ) {
						W += (INT_MAX+1);
						Carry = 1;
					}
					else Carry = 0;
					break;
				case '-':
					W = X - Y;
					if( W > INT_MAX ) {
						W -= (INT_MAX+1);
						Carry = 1;
					}
					else if( W < INT_MIN ) {
						W += (INT_MAX+1);
						Carry = 1;
					}
					else Carry = 0;
					break;
				case '*':
					W = X * Y;
					R[7] = W / (INT_MAX+1);
					R[8] = W % (INT_MAX+1);
					break;
				case '/':
					W = X / Y;
					R[7] = W;
					R[8] = X % Y;
					break;
			}
		}
		else if(args.length == 5 && args[0].equals("TestBit")) {
			// TestBit
			int rx = args[1].charAt(1)-'0';
			int i = Integer.parseInt(args[2]);
			int val = (1<<i) & R[rx];
			if(val > 0) Carry = 1;
			else Carry = 0;
		}
		else if(args.length == 5 && args[3].equals("->")) {
			// R[x] &|^ R[y] -> R[x]
			int a = args[0].charAt(1)-'0', b = args[2].charAt(1)-'0', c = args[4].charAt(1)-'0';
			switch( args[1].charAt(0) ) {
				case '&':
					R[c] = R[a] & R[b];
					break;
				case '|':
					R[c] = R[a] | R[b];
					break;
				case '^':
					R[c] = R[a] ^ R[b];
					break;
			}	
		}
		else if(arg.equals("JumpC")) {
			// JumpC
			if( Carry == 1 ) PC = MDR;
			else PC = PC + 1;
		}
		else if(arg.equals("JumpNC")) {
			// JumpNC
			if( Carry == 1 ) PC = PC + 1;
			else PC = MDR;
		}
		else if(args.equals("SP_INC")) {
			SP = SP + 1;
		}
		else if(args.equals("SP_DEC")) {
			SP = SP - 1;
		}
		else if(args[0].equals("RSF")) {
			int rx = args[1].charAt(1)-'0';
			int ry = args[2].charAt(1)-'0';
			R[rx] >>>= R[ry];
		}
		else if(args[0].equals("LSF")) {
			int rx = args[1].charAt(1)-'0';
			int ry = args[2].charAt(1)-'0';
			R[rx] <<= R[ry];
		}	
		else if(args[0].equals("RSF3")) {
			int rx = args[1].charAt(1)-'0';
			int ry = args[2].charAt(1)-'0';
			int rry = R[ry];
			long rxv = ((long)R[rx+0]<<32) + ((long)R[rx+1]<<16) + R[rx+2];
			rxv >>= rry;
			String out = Long.toBinaryString( rxv );
			while( out.length() < 64 ) {
				out = "0" + out;
			}
			R[rx+0] = Integer.parseInt( out.substring( 16, 32 ), 2 );
			R[rx+1] = Integer.parseInt( out.substring( 32, 48 ), 2 );
			R[rx+2] = Integer.parseInt( out.substring( 48, 64 ), 2 );
		}
		else if(args[0].equals("LSF3")) {
			int rx = args[1].charAt(1)-'0';
			int ry = args[2].charAt(1)-'0';
			int rry = R[ry];
			long rxv = ((long)R[rx+0]<<32) + ((long)R[rx+1]<<16) + R[rx+2];
			String out = Long.toBinaryString( rxv );
			while( out.length() < 64 ) {
				out = "0" + out;
			}
			R[rx+0] = Integer.parseInt( out.substring( 16, 32 ), 2 );
			R[rx+1] = Integer.parseInt( out.substring( 32, 48 ), 2 );
			R[rx+2] = Integer.parseInt( out.substring( 48, 64 ), 2 );
		}	
		repaint();	
	}
}
